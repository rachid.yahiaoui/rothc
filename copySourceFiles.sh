#!/bin/bash - 
#===============================================================================
#
#          FILE: copySourceFiles.sh
# 
#         USAGE: ./copySourceFiles.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: à utiliser avant de constriure le package InfosolR:
#         ./copySourceFile.sh; Rscript -e 'require(roxygen2);
#         roxygenize("infosolR",copy.package=FALSE)'; R CMD check infosolR; R CMD build infosolR      
#        AUTHOR: Manuel Martin
#  ORGANIZATION: 
#       CREATED: 04/18/2014 15:34
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

CM=~/dev/carbonModelling/scripts/R
packageName=RothC
packagePath=$CM/packages/RothCPackage/$packageName
testPath=$packagePath/tests/testthat
packSourcePath=$packagePath/carbonModels

## cleans
rm $packagePath/R/*.* $testPath/*.*

cd $CM/carbonModels
(echo '
# RothC.R -- R implementation of the RothC model ( 
# http://www.rothamsted.ac.uk/sustainable-soils-and-grassland-systems/rothamsted-carbon-model-rothc ) 
# 
# Copyright (C) 2016 INRA - Manuel Martin 
# All rights reserved. 
# This file is part of the RothC R package. 
# 
# This RothC R package is free software: you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version. 
# 
# RothC is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU General Public License for more details. 
# 
# You should have received a copy of the GNU General Public License 
# along with RothC R package.  If not, see <http://www.gnu.org/licenses/>. 
# Should you use this package, and please quote the publication
# Martin, M.P., Cordier, S., Balesdent, J., Arrouays, D.: Periodic solutions
# for soil carbon dynamics equilibriums with time-varying forcing variables.
# Ecol. Model. 204, 532-530 (2007), based on which the package was writen.

'
cat $CM/packages/RothCPackage/dataDescription.R RothCMineralizationMod.lib.r RothC.lib.r ecolModPaper/carbon12months.lib.r
) > $packagePath/R/RothC.R

cat RothCMineralizationMod.lib.test.r RothC.lib.test.r ecolModPaper/carbon12months.lib.test.r > $testPath/test-all.R

cd $packagePath
